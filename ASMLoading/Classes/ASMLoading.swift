//
//  ASMLoading.swift
//  ASMLoading
//
//  Created by Ps on 24/12/2563 BE.
//

import UIKit

private let globalShared = ASMLoading()
public class ASMLoading {
    
    var isPresen: Bool = true
    var mainViewController: WindowAlertPresentationController?
    var storeMsg: [WindowAlertPresentationController?] = []
    // var alertModel :  ASMPrivateViewController?
    var  button = UIButton(type: .system)
    
    public var option: ASMAlertOption = ASMAlertOption()
    public var title:ASMAlertCustomView?
    public func setupOption(option: ASMAlertOption){
        self.option = option
    }
    
    private func msgAlert(_ txt: String = "",
                         des: String = "",
                         animated: Bool = true,
                         type: ASMLoadingType = .alert,
                         button:[ASMAlertCustomView]? = nil,
                         completion: ((ASMAlertStatus) -> Void)? = nil) {
        if isPresen {
            beforRemove { _ in
                let alertModel = self.setupViewMsg(txt: txt ,
                                                   des: des,
                                                   type: type,
                                                   button: button)
                if let mainView = self.mainViewController {
                    self.storeMsg.append(mainView)
                    alertModel.bgcolor = UIColor.black.withAlphaComponent(0.4)
                    alertModel.buttonAction = {data  in
                            completion?(data ? .ok:.close)
                    }
                    
                    mainView.showPrivateView {
                        completion?(.didshow)
                    }
                }
            }}
    }
    
    private func showFullPage(_ txt: String = "", des: String = "", animated: Bool = true, completion: (() -> Void)? = nil) {
        if isPresen {
            beforRemove { _ in
                self.setupView(txt: txt, des: des, type: .full)
                
                if let mainView = self.mainViewController {
                    self.storeMsg.append(mainView)
                    mainView.showPrivateView {
                        completion?()
                    }
                } else {
                    completion?()
                }
            }}
    }
    
    private func setupView(txt: String ,
                   des: String ,
                   type: ASMLoadingType = .loading) {
        let bundle = Bundle(for: ASMPrivateViewController.self)
        let alertModel = ASMPrivateViewController(nibName: "ASMPrivateViewController", bundle: bundle )
        alertModel.textTitle = txt
        alertModel.texttDetail = des
        alertModel.type = type
        alertModel.alertOption = option
        self.mainViewController = WindowAlertPresentationController(alert: alertModel)
        self.mainViewController?.modalPresentationStyle = .fullScreen
        if let mainView = mainViewController {
            if #available(iOS 13, *) {
                let scene = UIApplication.shared.keyWindow?.windowScene
                mainView.install(becomeKey: false, scene: scene)
            } else {
                mainView.install(becomeKey: false)
            }
        }
    }
    
    private func setupViewMsg(txt: String,
                      des: String,
                      type: ASMLoadingType = .alert,
                      button:[ASMAlertCustomView]? = nil) -> ASMPrivateViewController {
        let bundle = Bundle(for: ASMPrivateViewController.self)
        var customOpetion : ASMAlertOption!
        customOpetion = option
        //   option.imgicon = UIImage(named: "error-ic")
        let alertModel = ASMPrivateViewController(nibName: "ASMPrivateViewController", bundle: bundle )
        alertModel.textTitle = txt
        alertModel.texttDetail = des
        alertModel.type = type
        if let item = button {
            customOpetion.button = item
        }
        alertModel.setupASMAlertOption(alertOption: customOpetion)
        
        self.mainViewController = WindowAlertPresentationController(alert: alertModel)
        self.mainViewController?.modalPresentationStyle = .fullScreen
        if let mainView = mainViewController {
            if #available(iOS 13, *) {
                let scene = UIApplication.shared.keyWindow?.windowScene
                mainView.install(becomeKey: false, scene: scene)
            } else {
                mainView.install(becomeKey: false)
            }
        }
        
        return alertModel
    }
    
    public func checkPreview() -> Bool {
        if  mainViewController?.getNowPresent() != nil {
            return true
        } else {
            return false
        }
    }
    
   
   public enum ASMAlertStatus {
        case didshow
        case ok
        case close
    }
    
 
    private func  beforRemove(  completionHandler: @escaping (Bool) -> Void) {
        let total = storeMsg.count
        if total > 1 {
            var i = 0
            for  element in storeMsg {
                if let mainView = element {
                    mainView.hiddenPrivateView {
                        mainView.removeFromParent()
                    }
                    if i >= total - 1 {
                        completionHandler(true)
                        break
                    }
                }
                i = i + 1
            }
        } else {
            completionHandler(true)
        }
    }
    
    public func getMainVIew() -> UIWindow? {
        return  self.mainViewController?.getWindowsView()
    }
    
    public  func hidden( animated: Bool = true, completion: (() -> Void)? = nil) {
        if storeMsg.count > 1 {
            beforRemove { _ in
                completion?()
            }
        } else {
            if let mainView = mainViewController {
                mainView.hiddenPrivateView {
                    completion?()
                }
            } else {
                completion?()
            }
        }
    }
}


extension ASMLoading {
    public static var shared: ASMLoading {
        return globalShared
    }
    
    public static func show(_ txt: String = "",
                            des: String = "",
                            type: ASMLoadingType = .alert,
                            button:[ASMAlertCustomView]? = nil,
                            animated: Bool = true,
                            completion: ((ASMAlertStatus) -> Void)? = nil) {
        globalShared.msgAlert(txt, des: des, animated: animated, type: type,button: button, completion: completion)
   }
    
    
    public static func showFullPage(_ txt: String = "",
                               des: String = "",
                               animated: Bool = true,
                               completion: (() -> Void)? = nil) {
        
        globalShared.showFullPage(txt, des: des, animated: animated, completion: completion)
    }
}


public struct ASMAlertOption {

    public var txtTitle: String = ""
    public var txtDetail: String = ""
    public var imgicon :UIImage?
    public var imgiconHeight:CGFloat?
    public var backgroundColor: UIColor = .white
    public var type: ASMLoadingType = .alert
    
    public var title:ASMAlertCustomView? = ASMAlertCustomView(textColor: .black,
                                                              backgroundColor:.clear,
                                                       type: .ok)
  
    public  var detail:ASMAlertCustomView? = ASMAlertCustomView(textColor: UIColor.black.withAlphaComponent(0.8),
                                                        backgroundColor:.clear,
                                                        type: .ok)
    
    public var button:[ASMAlertCustomView]? = [ASMAlertCustomView(text: "OK",
                                                                   textColor: .white,
                                                                   backgroundColor: UIColor(asmhex: "00A6E6"),
                                                                   type: .ok),
                                        ASMAlertCustomView(text: "Close",
                                                           textColor:.red,
                                                           backgroundColor:  UIColor(asmhex: "EBEBEB"),
                                                           type: .close )]
    public init(){ }
}


public struct ASMAlertCustomView {
    public var text:String = ""
    public var textColor:UIColor = .white
    public var textBackgroundColor:UIColor = .clear
    public var backgroundColor:UIColor = .white
    public var font:UIFont? =  .systemFont(ofSize: 18) // UIFont(name: "KrungthaiFast-Regular", size: 18) //
    public var type:ASMAlertButtonType = .ok
    public var alertType: [ASMLoadingType] = [.alert]
    
    public init(text:String = "",
                textColor:UIColor = .white,textBackgroundColor:UIColor = .clear,
                backgroundColor:UIColor = UIColor(asmhex: "00A6E6"),
                font:UIFont? =  .systemFont(ofSize: 18),
                type:ASMAlertButtonType = .ok,
                alertType: [ASMLoadingType] = [.alert]){
        self.text = text
        self.textColor = textColor
        self.textBackgroundColor = textBackgroundColor
        self.backgroundColor = backgroundColor
        self.font = font
        self.type = type
        self.alertType = alertType
      
    }
}

public enum ASMAlertButtonType {
    case ok
    case close
}


 extension UIColor {
    public convenience init(asmhex: String, alpha: CGFloat = 1 ) {
          var chars = Array(asmhex.hasPrefix("#") ? asmhex.dropFirst() : asmhex[...])
          switch chars.count {
          case 3: chars = chars.flatMap { [$0, $0] }
          case 6: break
          default:

            self.init(white: 0, alpha: 0)
            return
          }

          self.init(red: .init(strtoul(String(chars[0...1]), nil, 16)) / 255,
                  green: .init(strtoul(String(chars[2...3]), nil, 16)) / 255,
                   blue: .init(strtoul(String(chars[4...5]), nil, 16)) / 255,
                  alpha: alpha)
      }
}
