//
//  ASMPrivateViewController.swift
//  KTBApp
//
//  Created by Ps on 8/6/2563 BE.
//  Copyright © 2563 INTRANET KTB. All rights reserved.
//


import UIKit
import Lottie

public enum ASMLoadingType {
    case alert
    case loading
    case full
    case checkin
}



public class ASMPrivateViewController: UIViewController {
    
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var mainStack: UIStackView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var txtLoading: ASMAnimationLable!
    
    @IBOutlet weak var txtMsgTitle: UILabel!
    @IBOutlet weak var txtDetail: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var txtMsgStack: UIStackView!
    @IBOutlet weak var loadingStack: UIStackView!
    
    let animationView = AnimationView(name: "group16",
                                      bundle: Bundle(for: ASMPrivateViewController.self))
    
    @IBOutlet weak var logoIcon: UIImageView!
    @IBOutlet weak var mainIcon: UIImageView!
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var viewFullPage: UIImageView!
    @IBOutlet weak var fullLogoView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var bgIcon: UIView!
    @IBOutlet weak var bgLoadingView: UIView!
    
    @IBOutlet weak var mainStackWidth: NSLayoutConstraint!
    @IBOutlet weak var alertTextView: UIView!
    @IBOutlet weak var buttonOk: UIButton!
    
    var textTitle: String = ""
    var texttDetail: String = ""
    var typeIsFullpage: Bool = false
    var alertOption: ASMAlertOption?
    
    var bgcolor: UIColor?
    
    @IBOutlet weak var fullView: UIView!
    @IBOutlet weak var checkInView: UIView!
    @IBOutlet weak var logoStack: UIStackView!
    @IBOutlet weak var alertWidth: NSLayoutConstraint!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var buttonStack: UIStackView!
    let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
 
    public  var buttonAction :((_ result: Bool) -> Void)?
    
    var type: ASMLoadingType = .loading
    public var imgicon :UIImage?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        mainStack.alpha = 0
        txtMsgStack.isHidden  = true
        loadingStack.isHidden  = true
        animationView.alpha = 0
        animationView.backgroundColor = .clear
        animationView.loopMode = .loop
        animationView.tag = 294820
        checkInView.isHidden = true
        buttonCancel.isHidden = true
        buttonOk.isHidden = true
        blurView.backgroundColor = .clear// UIColor.white.withAlphaComponent(0.4)
      //  blurView.fixInView(mainView)
        blurView.alpha = 0
        setupView()
        
        
        setupMessageType()
      
    }
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        bgLoadingView.layer.cornerRadius = 10
        bgLoadingView.layer.shadowRadius = 4
        bgLoadingView.layer.shadowOpacity = 0.4
        bgLoadingView.layer.shadowColor = UIColor.gray.withAlphaComponent(1).cgColor
        bgLoadingView.backgroundColor = UIColor.white.withAlphaComponent(1)
        
        bgIcon.layer.cornerRadius = bgIcon.frame.width / 2
        bgIcon.clipsToBounds = true
        //  bgIcon.backgroundColor = .clear//  UIColor.white.withAlphaComponent(0.1)
        logoIcon.layer.cornerRadius = 10
        logoView.layer.cornerRadius = logoIcon.frame.width / 2
        logoView.layer.shadowRadius = 4
        logoView.layer.shadowOpacity = 0.4
        logoView.layer.shadowColor = UIColor.black.withAlphaComponent(0.8).cgColor
        
        buttonStack.layer.cornerRadius = 8
        
        showSpringAnimation()
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //  DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        //  }
        switch type {
        case .loading,.full:
            animationView.fadeIn( 0.5, delay: 0.0) { _ in
                self.animationView.play()
            }
        default:
            break
        }
      
    }
    func setupView() {
        if textTitle == "" {
            headerView.isHidden = true
        } else {
            headerView.isHidden = false
          //  txtLoading.text = textTitle
           
        }
        
        if texttDetail == "" {
            detailView.isHidden = true
        } else {
            detailView.isHidden = false
            txtDetail.text = texttDetail
        }
        
        self.mainIcon.alpha = 0
        self.logoIcon.alpha = 0
        
        let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        blurView.backgroundColor = .clear// UIColor.white.withAlphaComponent(0.4)
        blurView.fixInView(bgLoadingView)
        blurView.layer.cornerRadius = 15
        blurView.clipsToBounds = true
    }
    
   private func setupMessageType(){
        switch type {
        case .alert:
            setupMsgAlert()
            loadingStack.isHidden = true
            txtMsgStack.isHidden = false
            txtMsgTitle.text = textTitle
            if let icon = alertOption?.imgicon {
                img.image = icon
                checkInView.isHidden = false
                checkInView.heightAnchor.constraint(equalToConstant: 50).isActive = true
            }
        case .loading:
            setupLoadingView()
            loadingStack.isHidden = false
            txtMsgStack.isHidden = true
     
        case .full:
            setupFullView()
        case .checkin:
            setupMsgAlert()
            checkInView.isHidden = false
        }
    }
    @IBAction func btOkAction(_ sender: Any) {
        // ASMLoading.shared().hidden()
        buttonAction?(true)
        ASMLoading.shared.hidden()
    }
    
    @IBAction func btcloseAction(_ sender: Any) {
        buttonAction?(false)
        ASMLoading.shared.hidden()
    }
    @IBAction func closeAction(_ sender: Any) {
        buttonAction?(false)
        ASMLoading.shared.hidden()
    }
    
    func setupASMAlertOption( alertOption: ASMAlertOption) {
        self.alertOption = alertOption
    }
    
   
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

    func setupFullView() {
        fullLogoView.alpha = 1
        animationView.fixInView(fullLogoView)
        
        //        fadeIn(fullLogoView,0.5, delay: 0.0) { (value) in
        //
        //        }
        //
        
        // animationView.fixInView(fullLogoView)
        self.logoStack.isHidden = false
        self.fullView.isHidden = false
        self.buttonStack.isHidden = true
    }
    func setupMsgAlert() {
        if let bg = bgcolor {
            mainView.backgroundColor = bg
        }
     
        fullView.isHidden = true
        self.txtDetail.font.withSize(16)
        
        self.alertWidth.isActive = false
        buttonStack.isHidden = false
        logoStack.removeFromSuperview()
        
        let newConstraint = mainStackWidth.constraintWithMultiplier(0.7)
        view.removeConstraint(mainStackWidth)
        view.addConstraint(newConstraint)
        view.layoutIfNeeded()
        mainStackWidth = newConstraint
        
        guard let option = alertOption else{
            return
        }
        
        
        bgLoadingView.backgroundColor = option.backgroundColor
        txtMsgTitle.isHidden = false
        if let title = option.title {
            txtMsgTitle.textColor = title.textColor
            txtMsgTitle.backgroundColor = title.backgroundColor
            txtMsgTitle.font = title.font
        }
        
        if let detail = option.detail {
            txtDetail.textColor = detail.textColor
            txtDetail.backgroundColor = detail.backgroundColor
            txtDetail.font = detail.font
        }
        
        if let bt =  option.button {
      
        for  item in bt {
            switch item.type {
                case .ok:
                    buttonOk.setTitle( item.text, for: .normal)
                    buttonOk.titleLabel?.font = item.font
                    buttonOk.setTitleColor( item.textColor, for: .normal)
                    buttonOk.backgroundColor = item.backgroundColor
                    buttonOk.isHidden = false
                case .close:
                    buttonCancel.setTitle( item.text, for: .normal)
                    buttonCancel.titleLabel?.font = item.font
                    buttonCancel.setTitleColor( item.textColor, for: .normal)
                    buttonCancel.backgroundColor = item.backgroundColor
                    buttonCancel.isHidden = false
                }
            }
            
        }
    }
    
    func setupLoadingView() {
        self.alertWidth.constant = 140
        buttonStack.isHidden = true
        logoStack.isHidden = false
        fullView.isHidden = true
        
//                let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
//                blurView.backgroundColor = .clear// UIColor.white.withAlphaComponent(0.4)
//                fixInView(mainView: blurView,mainView)
        
        logoView.fadeIn(0.5, delay: 0.0) { _ in
        }
        animationView.fixInView(logoView)
        view.removeConstraint(mainStackWidth)
        view.layoutIfNeeded()
    }
    
   private func showSpringAnimation () {
        UIView.animate(withDuration:0.5) {
            self.mainStack.alpha = 1
            self.blurView.alpha = 1
        }
        // view.transform = view.transform.scaledBy(x: 0.7, y: 0.7)
        self.bgLoadingView.transform = self.bgLoadingView.transform.scaledBy(x: 0.7, y: 0.7)
        self.bgIcon.transform = self.bgIcon.transform.scaledBy(x: 0.7, y: 0.7)
        self.loadingStack.transform = self.loadingStack.transform.scaledBy(x: 0.7, y: 0.7)
        self.mainStack.transform = self.loadingStack.transform.scaledBy(x: 0.7, y: 0.7)
        UIView.animate(withDuration: 1.2,
                       delay: 0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 0.6,
                       options: .curveEaseInOut, animations: {
                        self.bgLoadingView.transform = CGAffineTransform.identity
                        self.bgIcon.transform = CGAffineTransform.identity
                        self.loadingStack.transform = CGAffineTransform.identity
                        self.mainStack.transform = CGAffineTransform.identity
                       }) { _ in
      
        }
    }
    
    public  func clearBG() {
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.isExclusiveTouch = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        self.navigationController?.toolbar.isHidden = true
        self.navigationController?.navigationBar.clipsToBounds = false
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "KrungthaiFast-Regular", size: 18) ?? UIFont.systemFont(ofSize: 18)]
    }

}




extension ASMLoading {
    
    
}
extension UIView {
    func dropShadow(superview: UIView) {
        // Get context from superview
        UIGraphicsBeginImageContext(self.bounds.size)
        superview.drawHierarchy(in: CGRect(x: -self.frame.minX, y: -self.frame.minY, width: superview.bounds.width, height: superview.bounds.height), afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        // Add a UIImageView with the image from the context as a subview
        let imageView = UIImageView(frame: self.bounds)
        imageView.image = image
        imageView.layer.cornerRadius = self.layer.cornerRadius
        imageView.clipsToBounds = true
        self.addSubview(imageView)
        
        // Bring the background color to the front, alternatively set it as UIColor(white: 1, alpha: 0.2)
        let brighter = UIView(frame: self.bounds)
        brighter.backgroundColor = self.backgroundColor ?? UIColor(white: 1, alpha: 0.2)
        brighter.layer.cornerRadius = self.layer.cornerRadius
        brighter.clipsToBounds = true
        self.addSubview(brighter)
        
        // Set the shadow
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 5)// (0, 5)
        self.layer.shadowOpacity = 0.35
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
    }
    
    public func fixInView(_ container: UIView!, index: Int? = 0, superView: UIView? = nil) {
        self.translatesAutoresizingMaskIntoConstraints = false

        self.frame = container.frame
        let mainView: UIView = superView == nil ? container:superView!
        if let index = index {
            mainView.insertSubview(self, at: index)
        } else {
            mainView.addSubview(self)
        }

        //  container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}



@IBDesignable
 class ASMAnimationLable: UILabel {
    
    @IBInspectable var startColor: UIColor = UIColor.white  {
        didSet {
            setupMask()
        }
    }
    
    @IBInspectable var centerColor: UIColor = UIColor.gray
    {
        didSet {
            setupMask()
        }
    }
    @IBInspectable var endColor: UIColor = UIColor.white
    {
        didSet {
            setupMask()
        }
    }

    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        setupMask()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupMask()
    }
    
    var gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        let locations = [0.25, 0.50, 0.75]
        gradientLayer.locations = locations as [NSNumber]?
        return gradientLayer
    }()
    
    override  var text: String? {
        didSet {
            if let text = text {
                if oldValue != text {
                    self.text = text
                    setupMask()
                }
            }
        }
    }
    
    func updatePaths() {
        gradientLayer.frame = CGRect(x: -bounds.size.width, y: bounds.origin.y, width: 2 * bounds.size.width, height: bounds.size.height)
     }
    
    override  func layoutSubviews() {
        super.layoutSubviews()
        updatePaths()
       }
    
    private func setupMask() {
        layer.sublayers?.removeAll()
        
       let image = UIImage().imageWithLabel(label: self)
        
        let maskLayer = CALayer()
        maskLayer.backgroundColor = UIColor.clear.cgColor
        maskLayer.frame = bounds.offsetBy(dx: bounds.size.width, dy: 0)
        maskLayer.contents = image?.cgImage
        gradientLayer.colors = [startColor.cgColor,
                                centerColor.cgColor,
                                endColor.cgColor ]
        gradientLayer.mask = maskLayer
        
        layer.sublayers = [gradientLayer]
        let gradientAnimation = CABasicAnimation(keyPath: "locations")
        gradientAnimation.fromValue = [0.0, 0.0, 0.25]
        gradientAnimation.toValue = [0.75, 1.0, 1.0]
        gradientAnimation.duration = 1.7
        gradientAnimation.repeatCount = Float.infinity
        gradientAnimation.isRemovedOnCompletion = false
        gradientAnimation.fillMode = CAMediaTimingFillMode.forwards
        gradientLayer.add(gradientAnimation, forKey: nil)
    }
    
    override  func didMoveToWindow() {
        super.didMoveToWindow()
        setupMask()
    }
}
@IBDesignable
class CircleView: UIView {
    let shapeLayer = CAShapeLayer()

    @IBInspectable var fillColor: UIColor = .blue { didSet { shapeLayer.fillColor = fillColor.cgColor } }

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        configure()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        updatePaths()
    }
}

private extension CircleView {
    func configure() {
        layer.addSublayer(shapeLayer)
        shapeLayer.fillColor = fillColor.cgColor
    }

    func updatePaths() {
        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        let radius = min(bounds.width, bounds.height) / 2
        shapeLayer.path = UIBezierPath(arcCenter: center, radius: radius, startAngle: 0, endAngle: 2 * .pi, clockwise: true).cgPath
    }
}
extension UILabel {
    func getRenderedImage() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0)
        let context = UIGraphicsGetCurrentContext()
        context!.saveGState()
        self.layer.render(in: context!)
        let renderedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return renderedImage
    }
}

extension UIImage {
    public func imageWithLabel(label: UILabel) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        label.layer.render(in: context)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}
