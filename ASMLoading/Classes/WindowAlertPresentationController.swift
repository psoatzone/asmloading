//
//  WindowAlertPresentationController.swift
//  ASMLoading
//
//  Created by Ps on 24/12/2563 BE.
//

import UIKit
import Lottie

class WindowAlertPresentationController: UIViewController {
    // MARK: - Properties
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private lazy var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
    private let alert: UIViewController?
    private let alertView: UIViewController?
    // MARK: - Initialization
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    init(alert: UIViewController? = nil, alertView: UIViewController? = nil) {
        self.alert = alert
        self.alertView = alertView
        super.init(nibName: nil, bundle: nil)
        
        self.window?.rootViewController = self
        
        if  self.alert != nil {
            self.window?.windowLevel = UIWindow.Level.alert + 1 //  UIWindow.Level.normal //
        } else if self.alertView != nil {
            self.window?.windowLevel =  UIWindow.Level.normal + 1 //UIWindow.Level.normal//
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("This initializer is not supported")
    }
    
    func install(becomeKey: Bool) {
        guard let window = window else { return }
        if becomeKey {
            window.makeKeyAndVisible()
        } else {
            window.isHidden = false
        }
    }
    
    @available(iOS 13, *)
    func install(becomeKey: Bool, scene: UIWindowScene?) {
        guard let window = window else { return }
        window.windowScene = scene
        if becomeKey {
            window.makeKeyAndVisible()
        } else {
            window.isHidden = false
        }
    }
    
    func uninstall() {
        window?.isHidden = true
        window = nil
    }
    
    // MARK: - Presentation
    func present(animated: Bool, completion: (() -> Void)?) {
        if let alertModel = alertView {
            present(alertModel, animated: animated, completion: completion)
        }
    }
    
    func getNowPresent() -> UIViewController? {
        if  window != nil {
            return alertView
        } else {
            return  nil
        }
    }
    // MARK: - Overrides
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag) {
            self.window = nil
            completion?()
        }
    }
    // MARK: - PrivateView
    
    func showPrivateView( completion: (() -> Void)? = nil) {
        if let itemView = self.window?.subviews.last?.viewWithTag(12_345_928) {
            self.showAnimationView(itemView: itemView) {
                completion?()
            }
        } else if  let model = self.alert, let viewItem = model.view {
            viewItem.frame = UIScreen.main.bounds
            viewItem.tag = 12_345_928
            viewItem.alpha = 0
            self.window?.addSubview(viewItem)
            self.showAnimationView(itemView: viewItem) {
                completion?()
            }
        }
    }
    
    func hiddenPrivateView(completion: (() -> Void)? = nil) {
        if let itemView = self.window?.subviews.last?.viewWithTag(12_345_928) {
            if let animaiontView = itemView.viewWithTag(294820) as? AnimationView {
                animaiontView.animationSpeed = 0.5
                itemView.fadeOut(0.2) { _ in
                    self.uninstall()
                    completion?()
                }
            } else {
               itemView.fadeOut(0.4){ _ in
                    self.uninstall()
                    completion?()
                }
            }
        } else {
            self.uninstall()
            completion?()
        }
    }
    
    func showAnimationView(itemView: UIView, completion: (() -> Void)? = nil) {
        self.window?.isHidden = false
        itemView.alpha = 0
        itemView.fadeIn(0.5){ _ in
                        completion?()
                    }
    }
    
    func hiddenPrivateViewNoAnimation(completion: (() -> Void)? = nil) {
        if let itemView = self.window?.subviews.last?.viewWithTag(12_345_928) {
            itemView.alpha = 0
            self.uninstall()
            completion?()
        } else {
            self.uninstall()
            completion?()
        }
    }
    public func getWindowsView() -> UIWindow? {
        return window
    }
//    func fadeIn(_ itemView: UIView, _ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, alpha: CGFloat = 1.0, completion: @escaping ((Bool) -> Void) = { (_: Bool) -> Void in }) {
//        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
//            itemView.alpha = alpha
//        }, completion: completion)  }
//
//    func fadeOut(_ itemView: UIView, _ duration: TimeInterval = 0.5, delay: TimeInterval = 1.0, alpha: CGFloat = 0.0, completion: @escaping (Bool) -> Void = { (_: Bool) -> Void in }) {
//        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseOut, animations: {
//            itemView.alpha = alpha
//        }, completion: completion)
//    }
}

extension UIView {

    public func fadeIn(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, alpha: CGFloat = 1.0, completion: @escaping ((Bool) -> Void) = { (_: Bool) -> Void in }) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = alpha
        }, completion: completion)  }

    public   func fadeOut(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, alpha: CGFloat = 0.0, completion: @escaping (Bool) -> Void = { (_: Bool) -> Void in }) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.alpha = alpha
        }, completion: completion)
    }
}
