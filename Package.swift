// swift-tools-version:5.0
import PackageDescription

let package = Package(
    name: "ASMLoading",
    platforms: [.iOS(.v11)],
    // platforms: [.iOS("9.0"), .macOS("10.10"), tvOS("9.0"), .watchOS("2.0")],
    products: [
        .library(name: "ASMLoading", targets: ["ASMLoading"])
    ],
    targets: [
        .target(
            name: "ASMLoading",
            path: "lottie-swift/src",
            exclude: ["Public/MacOS"]
        )
    ]
)
